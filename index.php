<!DOCTYPE html>
<html>
<head>
	<title>STOPKY</title>

	<script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			var counting = false;
			var stopky = null;

			function start_timer() {
				stopky = setInterval(function(){timer()}, 1000);
			}

			function stop_timer() {
				window.clearInterval(stopky);
			}

			function reset_timer() {
				localStorage.seconds = 0;
				render_timer();
			}

			function timer() {
				localStorage.seconds++;
				render_timer();
			}

			function render_timer() {
				$('#time').text(time_to_string(localStorage.seconds));
			}

			function time_to_string(time) {
				var sec_numb	= parseInt(time);
				var hours   = Math.floor(sec_numb / 3600);
				var minutes = Math.floor((sec_numb - (hours * 3600)) / 60);
				var seconds = sec_numb - (hours * 3600) - (minutes * 60);

				if (hours   < 10) {hours   = "0"+hours;}
				if (minutes < 10) {minutes = "0"+minutes;}
				if (seconds < 10) {seconds = "0"+seconds;}
				var time	= hours+':'+minutes+':'+seconds;
				return time;
			};

			render_timer();

			$('p').on('click', '.start.enabled', function(){
				start_timer();
				$(this).removeClass('enabled');
				$('.reset').removeClass('enabled');
				$('.stop').addClass('enabled');
			});

			$('p').on('click', '.stop.enabled', function(){
				stop_timer();
				$(this).removeClass('enabled');
				$('.reset').addClass('enabled');
				$('.start').addClass('enabled');
			});

			$('p').on('click', '.reset.enabled', function(){
				reset_timer();
			});

		});
	</script>

	<link type="text/css" rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700&amp;subset=latin,latin-ext">
	<style type="text/css">
		body {
			color: #444;
			background-color: #f3f3f3;
			font-family: 'Open Sans', Arial, Helvetica, sans-serif;
			font-size: 24px;
		}
		a {
			color: #aaa;
			text-decoration: none;
		}
		a:hover {
			text-decoration: underline;
		}
		a.enabled {
			color: #444;
		}
		.container {
			text-align: center;
		}
		.time {
			font-weight: bold;
		}
	</style>

</head>
<body>
<div class="container">
	<h1 id="time">00:00:00</h1>
	<p>
		<a class="start enabled" href="#">START</a>
		| <a class="reset enabled" href="#">RESET</a>
		| <a class="stop" href="#">STOP</a>
	</p>
</div>
</body>
</html>